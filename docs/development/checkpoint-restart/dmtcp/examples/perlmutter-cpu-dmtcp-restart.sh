#!/bin/bash
#SBATCH -J test_cr
#SBATCH -q debug
#SBATCH -N 1 
#SBATCH -C cpu
#SBATCH -t 00:10:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH --time-min=00:10:00

#for c/r with dmtcp
module load dmtcp nersc_cr

#checkpointing once every two minutes
start_coordinator -i 120

#restarting from dmtcp checkpoint files
./dmtcp_restart_script.sh 


