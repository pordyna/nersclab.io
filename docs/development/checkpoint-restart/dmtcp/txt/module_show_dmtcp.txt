elvis@perlmutter:login29:~> module show dmtcp
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   /global/common/software/nersc/pm-2022.12.0/modulefiles/dmtcp/3.0.0.lua:
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
whatis("Name : dmtcp")
whatis("Version : 3.0.0")
whatis("Target : zen3")
whatis("Short description : DMTCP is a tool for adding transparent checkpoint-restart capability to an existing dynamically linked application.")
help([[DMTCP is a tool for adding transparent checkpoint-restart capability to an existing dynamically linked application.]])
prepend_path("PATH","/global/common/software/nersc/pm-2022q4/sw/dmtcp-3.0.0/bin")
prepend_path("MANPATH","/global/common/software/nersc/pm-2022q4/sw/dmtcp-3.0.0/share/man")
prepend_path("CMAKE_PREFIX_PATH","/global/common/software/nersc/pm-2022q4/sw/dmtcp-3.0.0/lib")
prepend_path("LD_LIBRARY_PATH","/global/common/software/nersc/pm-2022q4/sw/dmtcp-3.0.0/")
setenv("DMTCP_DIR","/global/common/software/nersc/pm-2022q4/sw/dmtcp-3.0.0/")
setenv("DMTCP_DL_PLUGIN","0")
