# Abinit

[ABINIT](https://www.abinit.org) is a software suite to calculate the optical,
mechanical, vibrational, and other observable properties of materials. Starting
from the quantum equations of density functional theory, you can build
up to advanced applications with perturbation theories based on DFT,
and many-body Green's functions (GW and DMFT).

## Availability and Supported Architectures at NERSC

Abinit is [allowed](../../policies/software-policy/index.md) at NERSC and runs on CPUs.
Experimental GPU support is available.

## Application Information, Documentation, and Support

*  [Forum](https://forum.abinit.org)
*  [Wiki](https://wiki.abinit.org/doku.php)
*  [Mailing List](https://sympa-2.sipr.ucl.ac.be/abinit.org)

## Using Abinit at NERSC

See the [example jobs page](../../jobs/examples/index.md) for additional
examples and information about jobs.

## Building Abinit from Source

Users are welcome to build their own binaries from source.

## Related Applications

* [CP2K](../cp2k/index.md)
* [JDFTx](https://jdftx.org/)
* [Octopus](https://www.octopus-code.org/wiki/Main_Page)
* [PARSEC](http://real-space.org/)
* [RMGDFT](https://github.com/RMGDFT/rmgdft)
* [SIESTA](../siesta/index.md)
* [Quantum ESPRESSO](../quantum-espresso/index.md)
* [VASP](../vasp/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
