# Burst Buffer

!!! warning "Cori retired on May 31, 2023 at noon"
    	The Burst Buffer file system was retired along with
    	Cori. Please refer to the [Migrating from Cori to
    	Perlmutter](../systems/cori/migrate_to_perlmutter.md) page for
    	the detailed Cori retirement plan and information about
    	migrating your applications to Perlmutter.

The 1.8 PB NERSC Burst Buffer was based on Cray
[DataWarp](https://support.hpe.com/hpesc/public/
docDisplay?docId=a00113971en_us&page=Quick_Start_to_Using_DataWarp.html)
and used flash or SSD (solid-state drive) technology to significantly
increase the I/O performance on Cori. The peak bandwidth performance
was over 1.7 TB/s with each Burst Buffer node contributing up to 6.5
GB/s.
